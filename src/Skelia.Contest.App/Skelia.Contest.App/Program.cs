﻿using System;
using SkeliaBase;

namespace Skelia.Contest.App
{
    class Program
    {
        private const string FailureMessage = "You have failed....\nPress any key to continue";

        static void Main(string[] args)
        {
            try
            {
                var skeliaApp = new SkeliaApp();
                skeliaApp.Start();
            }
            catch (Exception ex)
            {
                DisplayFailure(ex);
            }
        }

        private static void DisplayFailure(Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine(FailureMessage);
            Console.ReadLine();
        }
    }
}
