#Welcome to the Skelia Poland contest#
- Please download the solution (prepared for Visual Studio 2015), and try to solve the tasks which are requested in the program.
- The program will provide feedback as soon it is run, but make sure to look at the code comments included in the referenced libraries to find out what needs to be implemented.
## Goal ##
- The goal is to perform the task requested using a minimal amount of characters.
- When the program is complete you will receive feedback on what to do next.
## Rules ##
- Any of the existing cs files in the solution cannot be modified.
- Extra libraries cannot be added to the project.
- We will only accept solutions received until 09 November 2016 23:59 CET.

**Good luck!**